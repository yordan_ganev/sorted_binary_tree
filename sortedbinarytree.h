/*
 *  Author: Yordan
 *  Date:   08.10 2020
 *
 *  About: Sorted binary tree library
 */

#ifndef SORTEDBINARYTREE_H
#define SORTEDBINARYTREE_H

#include <iostream>

using namespace std;

template <class T>
class CSortTree
{
private:
    struct Branch
    {
        T data;
        Branch* left = nullptr;
        Branch* right = nullptr;
    };
    Branch *root;

    // private funcion prototypes
    void _deleteNodeR(Branch *node);
    void _addData( Branch *node, const T& data);
    void _leftRootRight(Branch *node);

    // private flag
    bool _dataAdded;

public:
    CSortTree();
    ~CSortTree();

    // public function prototypes
    void push(const T& data);
    void display();

};


// Basic constructor
template <class T>
CSortTree<T>::CSortTree()
 : root(nullptr), _dataAdded(false)
{};

// Destructor
template <class T>
CSortTree<T>::~CSortTree()
{
    if (root == nullptr)
    {
        return;
    }

    _deleteNodeR(root);

};

/**
 * @brief delete node recursive (with nodes/branches behind)
 */
template <class T>
void CSortTree<T>::_deleteNodeR(Branch *node)
{
    if ( node->left != nullptr )
    {
        _deleteNodeR(node->left);
    }
    if ( node->right != nullptr )
    {
        _deleteNodeR(node->right);
    }

    delete node;
}

/**
 * @brief Print nodes data Left then current node, then Right nodes
 * @param node: current node
 */

template <class T>
void CSortTree<T>::_leftRootRight(Branch *node)
{
    if (node->left != nullptr)
    {
        _leftRootRight(node->left);
    }

    cout << node->data << " ";

    if (node->right != nullptr)
    {
        _leftRootRight(node->right);
    }
};


/**
 * @brief Recursion till reaching leaf (left < node < right) within data range.
 * Create new leaf and set data. Stop callings.
 *
 * @param node new current possition
 * @param data value unchanged every call
 */
template <class T>
void CSortTree<T>::_addData( Branch *node, const T& data)
{
    if (_dataAdded == true)
    {
        return;
    }

    // Search left
    if (node->data >= data)
    {
        if ( node->left != nullptr)
        {
            _addData(node->left, data);
        }
        else
        {
            node->left = new Branch;
            node->left->data = data;
            _dataAdded = true;
        }
    }

    // Search right
    if (node->data < data)
    {
        if (node->right != nullptr)
        {
            _addData(node->right, data);
        }
        else
        {
            node->right = new Branch;
            node->right->data = data;
            _dataAdded = true;
        }
    }
};


/**
 * @brief Sorted binary tree element push:
 * Place value on left if data <= Node-data or
 *             on right if data > Node-data.
 * Create new node with data.
 * Break function calls when data is added to the tree.
 */
template <class T>
void CSortTree<T>::push(const T& data)
{

    if ( root == nullptr)
    {
        root = new Branch;
        root->data = data;
        return;
    }

    _dataAdded = false;
    _addData(root, data);
}

/**
 * @brief Print elements in sorted order with recursion (min to max)
 */
template <class T>
void CSortTree<T>::display()
{
    if ( root != nullptr)
    {
        _leftRootRight(root);
        cout << endl;
    }
}

#endif // SORTEDBINARYTREE_H
