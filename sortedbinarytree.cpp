/*
 *  Author: Yordan
 *  Date:   08.10 2020
 *
 *  About: Sorted binary tree library
 */

// --- Libraries ---
#include <iostream>
#include <string>
#include <string.h>
#include "sortedbinarytree.h"

using namespace std;

int main()
{
    // Get nummbers
    CSortTree<float>tree;

    string input;

    cout << "Enter number: ";
    getline(cin, input);

    while ( input.length() )
    {
        tree.push(stof(input));

        cout << "Enter new line to exit or continue entering numbers: ";
        getline(cin, input);
    }

    // Display sorted
    tree.display();
    return 0;
}
